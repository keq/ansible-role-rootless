---
- name: Check if nettle >= 3.6 is available
  shell: "pkg-config --exists 'nettle >= 3.6'"
  register: nettle_exists
  ignore_errors: yes
- name: Print `nettle_exists`
  debug:
    var: nettle_exists.rc

- name: Ensure nettle 3.7.2
  when: "nettle_exists.rc != 0"
  block:
  - name: Download nettle source archive
    get_url:
      url: "https://ftp.gnu.org/gnu/nettle/nettle-3.7.2.tar.gz"
      dest: "{{ build_dir }}/nettle-3.7.2.tar.gz"
      force: no
  - name: Extract nettle archive
    unarchive:
      src: "{{ build_dir }}/nettle-3.7.2.tar.gz"
      dest: "{{ build_dir }}/"
      remote_src: yes
      creates: "{{ build_dir }}/nettle-3.7.2/"
  - name: Check nettle config.log for exit status
    shell:
      chdir: "{{ build_dir }}/nettle-3.7.2/"
      cmd: "tail -n 1 config.log"
    register: nettle_config_exit_status
    ignore_errors: yes
  - name: Print nettle configure exit status
    debug:
      var: nettle_config_exit_status["stdout"]
  - name: Configure nettle
    when: "nettle_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/nettle-3.7.2/"
      cmd: ./configure --prefix="{{ install_prefix }}"
  - name: Make install nettle
    make:
      chdir: "{{ build_dir }}/nettle-3.7.2/"
      target: install

- name: Check if libtasn1.pc is available
  shell: "pkg-config --libs libtasn1"
  register: libtasn1_pc
  ignore_errors: yes
- name: Print libtasn1.pc location
  debug:
    var: libtasn1_pc['stdout']

- name: Ensure libtasn1 4.16.0
  when: "not libtasn1_pc['stdout']"
  block:
  - name: Download libtasn1 source archive
    get_url:
      url: "https://ftp.gnu.org/gnu/libtasn1/libtasn1-4.16.0.tar.gz"
      dest: "{{ build_dir }}/libtasn1-4.16.0.tar.gz"
      force: no
  - name: Extract libtasn1 archive
    unarchive:
      src: "{{ build_dir }}/libtasn1-4.16.0.tar.gz"
      dest: "{{ build_dir }}/"
      remote_src: yes
      creates: "{{ build_dir }}/libtasn1-4.16.0/"
  - name: Check libtasn1 config.log for exit status
    shell:
      chdir: "{{ build_dir }}/libtasn1-4.16.0/"
      cmd: "tail -n 1 config.log"
    register: libtasn1_config_exit_status
    ignore_errors: yes
  - name: Print libtasn1 configure exit status
    debug:
      var: libtasn1_config_exit_status["stdout"]
  - name: Configure libtasn1
    when: "libtasn1_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/libtasn1-4.16.0/"
      cmd: ./configure --prefix="{{ install_prefix }}" --disable-static
  - name: Make install libtasn1
    make:
      chdir: "{{ build_dir }}/libtasn1-4.16.0/"
      target: install

- name: Check if libidn2.pc is available
  shell: "pkg-config --libs libidn2"
  register: libidn2_pc
  ignore_errors: yes
- name: Print libidn2.pc location
  debug:
    var: libidn2_pc['stdout']

- name: Ensure libidn2 2.3.0
  when: "not libidn2_pc['stdout']"
  block:
  - name: Download libidn2 source archive
    get_url:
      url: "https://ftp.gnu.org/gnu/libidn/libidn2-2.3.0.tar.gz"
      dest: "{{ build_dir }}/libidn2-2.3.0.tar.gz"
      force: no
  - name: Extract libidn2 archive
    unarchive:
      src: "{{ build_dir }}/libidn2-2.3.0.tar.gz"
      dest: "{{ build_dir }}/"
      remote_src: yes
      creates: "{{ build_dir }}/libidn2-2.3.0/"
  - name: Check libidn2 config.log for exit status
    shell:
      chdir: "{{ build_dir }}/libidn2-2.3.0/"
      cmd: "tail -n 1 config.log"
    register: libidn2_config_exit_status
    ignore_errors: yes
  - name: Print libidn2 configure exit status
    debug:
      var: libidn2_config_exit_status["stdout"]
  - name: Configure libidn2
    when: "libidn2_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/libidn2-2.3.0/"
      cmd: ./configure --prefix="{{ install_prefix }}" --disable-static
  - name: Make install libidn2
    make:
      chdir: "{{ build_dir }}/libidn2-2.3.0/"
      target: install

- name: Check if p11-kit.pc is available
  shell: "pkg-config --libs p11-kit-1"
  register: p11kit_pc
  ignore_errors: yes
- name: Print p11-kit.pc location
  debug:
    var: p11kit_pc['stdout']

- name: Ensure p11-kit 0.23.22
  when: "not p11kit_pc['stdout']"
  block:
  - name: Download p11-kit source archive
    get_url:
      url: "https://github.com/p11-glue/p11-kit/releases/download/0.23.22/p11-kit-0.23.22.tar.xz"
      dest: "{{ build_dir }}/p11-kit-0.23.22.tar.xz"
      force: no
  - name: Extract p11-kit archive
    unarchive:
      src: "{{ build_dir }}/p11-kit-0.23.22.tar.xz"
      dest: "{{ build_dir }}/"
      remote_src: yes
      creates: "{{ build_dir }}/p11-kit-0.23.22/"
  - name: Check p11-kit config.log for exit status
    shell:
      chdir: "{{ build_dir }}/p11-kit-0.23.22/"
      cmd: "tail -n 1 config.log"
    register: p11kit_config_exit_status
    ignore_errors: yes
  - name: Print p11-kit configure exit status
    debug:
      var: p11kit_config_exit_status["stdout"]
  - name: Configure p11-kit
    when: "p11kit_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/p11-kit-0.23.22/"
      cmd: ./configure --prefix="{{ install_prefix }}" --disable-static
  - name: Make install p11-kit
    make:
      chdir: "{{ build_dir }}/p11-kit-0.23.22/"
      target: install

- name: Check gnutls version
  shell: "gnutls-cli -v | head -n 1 | cut -d ' ' -f 2-"
  register: gnutls_version
  ignore_errors: yes
- name: Print gnutls version
  debug:
    var: gnutls_version["stdout"]

- name: Ensure gnutls 3.7.1
  when: "gnutls_version['stdout'] != '3.7.1'"
  block:
  - name: Download gnutls source archive
    get_url:
      url: https://www.gnupg.org/ftp/gcrypt/gnutls/v3.7/gnutls-3.7.1.tar.xz
      dest: "{{ build_dir }}/gnutls-3.7.1.tar.xz"
      force: no
  - name: Extract gnutls archive
    unarchive:
      src: "{{ build_dir }}/gnutls-3.7.1.tar.xz"
      dest: "{{ build_dir }}/"
      remote_src: yes
      creates: "{{ build_dir }}/gnutls-3.7.1/"
  - name: Check gnutls config.log for exit status
    shell:
      chdir: "{{ build_dir }}/gnutls-3.7.1/"
      cmd: "tail -n 1 config.log"
    register: gnutls_config_exit_status
    ignore_errors: yes
  - name: Print gnutls configure exit status
    debug:
      var: gnutls_config_exit_status["stdout"]
  - name: Configure gnutls
    when: "gnutls_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/gnutls-3.7.1/"
      cmd: ./configure --prefix="{{ install_prefix }}" --with-included-unistring
  - name: Make install gnutls
    make:
      chdir: "{{ build_dir }}/gnutls-3.7.1/"
      target: install

- name: Check emacs version
  shell: "emacs --version | head -n 1 | cut -d ' ' -f 3-"
  register: emacs_version
  ignore_errors: yes
- name: Print emacs version
  debug:
    var: emacs_version["stdout"]

- name: Ensure emacs from master branch
  when: not emacs_version['stdout'] or emacs_version['stdout'] is version('29', '<')
  block:
  - name: Clone emacs git repo
    git:
      repo: "git://git.savannah.gnu.org/emacs.git"
      dest: "{{ build_dir }}/emacs"
      version: master
      update: no
  - name: Check emacs config.log for exit status
    shell:
      chdir: "{{ build_dir }}/emacs/"
      cmd: "tail -n 1 config.log"
    register: emacs_config_exit_status
    ignore_errors: yes
  - name: Print emacs configure exit status
    debug:
      var: emacs_config_exit_status["stdout"]
  - name: Configure emacs
    when: "emacs_config_exit_status['stdout'] != 'configure: exit 0'"
    shell:
      chdir: "{{ build_dir }}/emacs/"
      cmd: >
        ./autogen.sh git &&
        ./autogen.sh autoconf && 
        ./configure --prefix="{{ install_prefix }}" --without-x
        --with-x-toolkit=no --without-xft --without-lcms2
        --without-rsvg
  - name: Make install emacs
    make:
      chdir: "{{ build_dir }}/emacs/"
      target: install
